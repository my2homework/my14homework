﻿
#include <iostream>


class Vector
{

private: 
    double x, y, z;

public:
    Vector() : x(), y(), z()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        double sum;
        sum = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
        std::cout << x << ' ' << y << ' ' << z << "\n" << "Modul raven: " << sum;
        
    }

};
int main()
{
    Vector v(2,4,4);
    v.Show();
}
 
